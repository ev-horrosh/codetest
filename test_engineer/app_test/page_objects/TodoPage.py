from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


class Todo:
    text_header_xpath = '//h1[@class="Todo-header"]'
    textbox_add_todo_xpath = '//input[@class="form-control"]'
    button_add_todo_xpath = '//button[@class="submit"]'
    list_tasks_xpath = '//li[@class="Todo-item"]'
    list_tasks_names_xpath = '//div[@class="done" or @class="undone"]'

    def __init__(self, driver):
        self.driver = driver

    @property
    def submit_button(self):
        return self.driver.find_element(By.XPATH, self.button_add_todo_xpath)

    @property
    def get_header_element(self):
        return self.driver.find_element(By.XPATH, self.text_header_xpath)

    @property
    def task_list(self):
        return self.driver.find_elements(By.XPATH, self.list_tasks_xpath)

    @property
    def task_names_list(self):
        task_names_elements = self.driver.find_elements(
            By.XPATH, self.list_tasks_names_xpath)
        return [task.text for task in task_names_elements]

    def refresh(self):
        self.driver.refresh()

    def _send_to_text_box(self, task):
        textbox = self.driver.find_element(
            By.XPATH, self.textbox_add_todo_xpath)
        textbox.clear()
        textbox.send_keys(task)
        return textbox

    def add_task_by_pressing_return(self, task):
        self._send_to_text_box(task).send_keys(Keys.RETURN)

    def add_task_by_submitting_form(self, task):
        self._send_to_text_box(task).submit()

    def add_task_button_click(self, task):
        self._send_to_text_box(task)
        self.submit_button.click()

    def get_completed_task_element(self, task):
        completed_task_element = self.driver.find_element(
            By.XPATH, f'//div[text()="{task}" and @class="done"]/preceding-sibling::span[text()="[X]"]')
        return completed_task_element

    def get_incompleted_task_element(self, task):
        incompleted_task_element = self.driver.find_element(
            By.XPATH, f'//div[text()="{task}" and @class="undone"]/preceding-sibling::span[text()="[  ]"]')
        return incompleted_task_element

    def is_complete_task(self, task):
        return bool(self.get_completed_task_element(task))

    def is_incomplete_task(self, task):
        return bool(self.get_incompleted_task_element(task))

    def uncomplete_task(self, task):
        uncomplete_task_element = self.driver.find_element(
            By.XPATH, f'//div[text()="{task}" and @class="done"]/preceding-sibling::span[text()="[X]"]')
        return uncomplete_task_element.click()

    def complete_task(self, task):
        incompleted_task_element = self.get_incompleted_task_element(task)
        incompleted_task_element.click()

    def get_value_of_css_property(self, task, property):
        completed_element = self.driver.find_element(
            By.XPATH, f'//span[text()="[X]"]/following-sibling::div[text()="{task}" and @class="done"]')
        return completed_element.value_of_css_property(property)

    def delete_task(self, task):
        self.driver.find_element(
            By.XPATH, f'//div[text()="{task}" and (@class="done" or @class="undone")]/following-sibling::button[@class="close"]').click()
