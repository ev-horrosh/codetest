## manual test casses

Verify if the todo box stretches to accommodate long text without going outside the box.
Verify if long text does not go outside the box.
Verify that the "add new todo" feature does not go off the screen even after adding a lot of tasks.
Verify that the color of the text is distinct and not too close to the background color, making it hard to read.
Verify that only one task can be deleted at a time.
Verify that the app does not crash when deleting a task.
Verify that the app functions properly with different window sizes.
Verify that data does not persist and the todo list resets after updating the page.


future notes

Verify that completed tasks are kept in the "completed" tab or on the top of the list.
Verify that tasks contain the date and time they were added.
Verify that the app functions properly when scaled up and tested in multiple browsers and devices using BrowserStack.