## Manual QA Check List

Launch the To-Do List application in your web browser.

Verify that the application has a header that reads "To-Do List".

Verify that the application has an input field for adding new To-Do items.

Verify that the application has a "+" button for submitting new To-Do items.

Add a new To-Do item by entering text into the input field and clicking the "+" button.

Verify that the new To-Do item is displayed in the To-Do list.

Verify that each To-Do item has a checkbox for marking it as complete.

Mark one of the To-Do items as complete by clicking its checkbox.

Verify that the marked To-Do item is displayed with a "done" label.

Verify that the marked To-Do item can be removed from the list by clicking the "x" button.

Repeat steps 6-9 for each of the To-Do items.

Verify that the To-Do list is empty after all items have been marked as complete and removed.

Verify that the text color of the To-Do items is clearly distinguishable from the background color.

Verify that the To-Do items are displayed within the boundaries of the box, and that long text does not overflow outside the box.

Verify that the application does not crash when deleting a To-Do item.

Verify that the To-Do items persist even after the application is updated or the page is refreshed.

Verify that completed To-Do items are kept in the "Completed" tab or are displayed on the top of the To-Do list.

Verify that the To-Do items have the date and time of completion displayed next to them.

Verify the To-Do List application on different browsers and devices by testing it with various window sizes and scaling.