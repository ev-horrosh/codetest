import pytest
from utils.read_properties import ReadConfig
from utils.app_logger import Log


class TestTodo:

    URL = ReadConfig.get_url()
    logger = Log.generate()

    def test_todo_header_is_displayed(self, setup):
        self.logger.info('-- test_todo_header_is_displayed --')
        todo = setup
        todo.driver.get((self.URL))
        if todo.get_header_element.text == 'Todo list' and todo.get_header_element.is_displayed():
            self.logger.info('-- verifying header is displayed --')
            assert True
        else:
            self.logger.error('-- header is not displayed --')
            assert False

    @pytest.mark.parametrize("task", [''])
    def test_add_empty_task(self, task, setup):
        self.logger.info('-- test_add_empty_task --')
        todo = setup
        todo.driver.get(self.URL)
        list_before = len(todo.task_list)
        todo.add_task_by_submitting_form(task)
        list_after = len(todo.task_list)
        if list_before == list_after:
            self.logger.info('-- empty task was not added --')
            assert True
        else:
            self.logger.error('-- empty task not added --')
            assert False

    @pytest.mark.parametrize("task", ['read a book'])
    def test_add_task_by_submitting_form(self, task, setup):
        self.logger.info('-- test_add_task_by_submitting_form --')
        todo = setup
        todo.driver.get(self.URL)
        list_before = len(todo.task_list)
        todo.add_task_by_submitting_form(task)
        list_after = len(todo.task_list)
        if list_before == list_after-1:
            self.logger.info('-- the form is submitted --')
            assert True
        else:
            self.logger.error('-- the form is not submitted --')
            assert False

    @pytest.mark.parametrize("task", ['read a book'])
    def test_add_task_by_pressing_return(self, task, setup):
        self.logger.info('-- test_add_task_keyboard --')
        todo = setup
        todo.driver.get(self.URL)
        list_before = len(todo.task_list)
        todo.add_task_by_pressing_return(task)
        list_after = len(todo.task_list)
        if list_before == list_after-1:
            self.logger.info('-- task was added --')
            assert True
        else:
            self.logger.error('-- task was not added --')
            assert False

    @pytest.mark.parametrize("task", ['read NLP book'])
    def test_add_task_by_button_click(self, task, setup):
        self.logger.info('-- test_add_task_by_button_click --')
        todo = setup
        todo.driver.get(self.URL)
        list_before = len(todo.task_list)
        todo.add_task_button_click(task)
        list_after = len(todo.task_list)
        if list_before == list_after-1:
            self.logger.info('-- task was added --')
            assert True
        else:
            self.logger.error('-- task was not added --')
            assert False

    @pytest.mark.parametrize("task", ['get up and stretch'])
    def test_if_same_task_can_be_added(self, task, setup):
        self.logger.info('-- test if same task can be added --')
        todo = setup
        todo.driver.get(self.URL)
        list_before = len(todo.task_list)
        todo.add_task_by_submitting_form(task)
        todo.add_task_by_submitting_form(task)
        list_after = len(todo.task_list)
        if list_after == list_before+2:
            self.logger.info('-- same task can be added --')
            assert True
        else:
            self.logger.error('-- same task can not be added --')
            assert False

    @pytest.mark.parametrize("task", ['read NLP book'])
    def test_if_added_task_is_not_complete(self, task, setup):
        self.logger.info('-- test_if_added_task_is_not_complete --')
        todo = setup
        todo.driver.get(self.URL)
        todo.add_task_by_submitting_form(task)
        if todo.is_incomplete_task(task):
            self.logger.info('-- task added_is_not_complete --')
            assert True
        else:
            self.logger.error('-- task added_is_complete --')
            assert False

    @pytest.mark.parametrize("task", ['write an article'])
    def test_added_task_goes_on_top(self, task, setup):
        self.logger.info('-- test_added_task_goes_on_top --')
        todo = setup
        todo.driver.get(self.URL)
        todo.add_task_by_submitting_form(task)
        if todo.task_names_list[0] == task:
            self.logger.info('-- added task goes on top of the list --')
            assert True
        else:
            self.logger.error('-- added task goes on top of the list --')
            assert False

    @pytest.mark.parametrize("task", ['write an article'])
    def test_uncompleted_task_goes_on_top(self, task, setup):
        self.logger.info('-- test_uncompleted_task_goes_on_top --')
        todo = setup
        todo.driver.get(self.URL)
        todo.add_task_by_submitting_form(task)
        todo.complete_task(task)
        todo.uncomplete_task(task)
        if todo.task_names_list[0] == task:
            self.logger.info('-- uncompleted task goes on top of the list --')
            assert True
        else:
            self.logger.error('-- uncompleted task goes on top of the list --')
            assert False

    @pytest.mark.parametrize("task", ['deploy an app'])
    def test_task_completion(self, task, setup):
        self.logger.info('-- test_task_completion --')
        todo = setup
        todo.driver.get(self.URL)
        todo.add_task_by_submitting_form(task)
        todo.complete_task(task)
        if todo.is_complete_task(task):
            self.logger.info('-- task is completed --')
            assert True
        else:
            self.logger.error('-- task is not completed --')
            assert False

    @pytest.mark.parametrize("task", ['deploy an app'])
    def test_task_uncompletion(self, task, setup):
        self.logger.info('-- test_task_completion --')
        todo = setup
        todo.driver.get(self.URL)
        todo.add_task_by_submitting_form(task)
        todo.complete_task(task)
        if todo.is_complete_task(task):
            self.logger.info('-- task is completed --')
            assert True
        else:
            self.logger.error('-- task is not completed --')
            assert False

    @pytest.mark.parametrize("task", ['deploy an app'])
    def test_completed_task_crossed_out(self, task, setup):
        self.logger.info('-- test_completed_task_crossed_out --')
        todo = setup
        todo.driver.get(self.URL)
        todo.add_task_by_submitting_form(task)
        todo.complete_task(task)
        todo.get_value_of_css_property(task, "text-decoration")
        if 'line-through' in todo.get_value_of_css_property(task, "text-decoration"):
            self.logger.info('-- task is crossed out --')
            assert True
        else:
            self.logger.error('-- task is not crossed out --')
            assert False

    @pytest.mark.parametrize("task", ['write an article'])
    def test_completed_task_goes_at_bottom_(self, task, setup):
        self.logger.info('-- test_completed_task_goes_at_bottom --')
        todo = setup
        todo.driver.get(self.URL)
        todo.add_task_by_submitting_form(task)
        todo.complete_task(task)
        if todo.task_names_list[-1] == task:
            self.logger.info(
                '-- completed task is at the bottom of the list --')
            assert True
        else:
            self.logger.error(
                '-- completed task is not at the bottom of the list --')
            assert False

    @pytest.mark.parametrize("task", ['10 jumping jacks'])
    def test_delete_completed_task(self, task, setup):
        self.logger.info('-- test_completed_task_goes_at_bottom --')
        todo = setup
        todo.driver.get(self.URL)
        tasks_before = todo.task_names_list
        todo.add_task_by_submitting_form(task)
        todo.complete_task(task)
        todo.delete_task(task)
        tasks_after = todo.task_names_list
        if tasks_before == tasks_after:
            self.logger.info('-- completed task was deleted --')
            assert True
        else:
            self.logger.error('-- completed task was not deleted --')
            assert False

    @pytest.mark.parametrize("task", ['10 jumping jacks'])
    def test_delete_incompleted_task(self, task, setup):
        self.logger.info('-- test_incompleted_task_goes_at_bottom --')
        todo = setup
        todo.driver.get(self.URL)
        tasks_before = todo.task_names_list
        todo.add_task_by_submitting_form(task)
        todo.delete_task(task)
        tasks_after = todo.task_names_list
        if tasks_before == tasks_after:
            self.logger.info('-- incompleted task was deleted --')
            assert True
        else:
            self.logger.error('-- incompleted task was not deleted --')
            assert False

    @pytest.mark.parametrize("task", ['oh'])
    def test_delete_incompleted_task(self, task, setup):
        self.logger.info('-- test_incompleted_task_goes_at_bottom --')
        todo = setup
        todo.driver.get(self.URL)
        tasks_before = todo.task_names_list
        todo.add_task_by_submitting_form(task)
        todo.driver.save_screenshot('./screenshots/01_task_added.png')
        todo.add_task_by_submitting_form(task)
        todo.driver.save_screenshot('./screenshots/02_another_task_added.png')
        todo.delete_task(task)
        todo.driver.save_screenshot('./screenshots/itemdeleted.png')
        tasks_after = todo.task_names_list
        if tasks_before == tasks_after:
            self.logger.info('-- incompleted task was deleted --')
            assert True
        else:
            self.logger.error('-- incompleted task was not deleted --')
            assert False
