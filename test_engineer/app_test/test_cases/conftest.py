from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager, IEDriverManager
import pytest
from page_objects.TodoPage import Todo


@pytest.fixture()
def setup(browser):
    if browser == 'chrome':
        s = Service(ChromeDriverManager().install())
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        todo = Todo(webdriver.Chrome(service=s, options=options))
    elif browser == 'firefox':
        s = Service(GeckoDriverManager().install())
        options = webdriver.FirefoxOptions()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        todo = Todo(webdriver.Firefox(service=s, options=options))
    yield todo
    todo.driver.close()


def pytest_configure(config):
    config._metadata['Project Name'] = 'TODO App'
    config._metadata['Tester'] = 'Ev'


@pytest.mark.optionalhook
def pytest_metadata(metadata):
    metadata.pop('Plugins', None)
    metadata.pop('JAVA_HOME', None)


def pytest_addoption(parser):
    parser.addoption('--browser')


@pytest.fixture()
def browser(request):
    return request.config.getoption('--browser')
