import configparser

config=configparser.RawConfigParser()
config.read('./configs/config.ini')


class ReadConfig:
    @staticmethod
    def get_url():
        return config.get('common info','url')